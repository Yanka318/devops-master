# Devops Concept

## Devops stack

My devops stack provides [Terraform](https://www.terraform.io/) for infrastructure as code, 
[GitLab](https://gitlab.com/) for version control and continuous integration, 
[Google Coud Plattform](https://cloud.google.com/gcp/) for scalable and managed services, Docker for containerization, 
and [Cloud Run](https://cloud.google.com/run?hl=de) for serverless deployment, 
ensuring an efficient and streamlined development and deployment pipeline.

## Goals

### Environments Transition:

- Seamless transition between development (dev) and production (prod) environments is the primary goal.

### CI/CD Pipeline:

- Orchestrated by GitLab.
- Three stages: Test, Build, and Deploy.
- Early issue identification in the development cycle.

### Infrastructure Management:

- Terraform for cloud infrastructure management.
- Aims for automated and reproducible allocation of infrastructure components.

### Zero-Downtime Deployment:

- Achieved through Cloud Run's traffic-splitting feature.
- Allows updating or releasing new application versions without disruptions.

### Monitoring:

- Google Cloud Monitoring and Logging services.
- Enables viewing and analysis of performance metrics and logs from Cloud Run.
- Ensures reliability, performance, and security of applications and infrastructure.

### Containerization:

- Cloud Run for containerization.
- Ensures consistency across different environments.


## General Preperations
- **Account setup:** Create a Google Cloud Platform (GCP) account.
  Set up service accounts and configure the necessary permissions for GitLab, Terraform, and other services.

## Automation

The source code will be managed using Git and hosted on GitLab. 
GitLab Container registry will be used to store the container images. 
The automation pipeline will execute the following tasks: 

1. `Test stage`
   
   **condition:** when merge request is created
   - pipeline is checking the code from the gitlab repository
   - necessary dependencies required for testing will be installed
   - unit testing to validate the functionality 
   - integration testing to ensure that different components work together as expected
   - code quality analysis
   - test coverage
   - stop if tests fail
2. `Build stage`

   **condition:** when pushed on develop
   - Build the docker image of the application to be deployed
   - tagging the image
   - push to gitlab container registry
   - optional cleanup of artifacts
3. `Deploy Stage`

   **condition:** when pushed on develop
   - deploy the container image to the `dev` and the `prod` environment when building the `develop` branch 
   for dev and the `main` branch for prod.
   - deploy the docker image to cloud run 
   - Utilize the traffic-splitting feature to ensure zero-downtime deployment
   - Set up monitoring and logging configurations for the deployed application using 
   Google Cloud Monitoring and Logging services.
In order to reach zero-downtime-deployment I will use the traffic-splitting-feature from cloud run, 
   which will allow to gradually move from one version to the next and avoid downtown. 

## Deployment

One goal is to achieve a seamless transition between 2 environments, deployment (dev) and production (prod). 
The dev environment will contain the latest pre-release build and the prod environment 
will contain the latest released build. The application will be deployed to the **Google** cloud platform. 

Infrastructure will be configured using **Terraform**. When using docker containerization 
there will be no need for virtual machines because the application will be encapsulated and the 
docker containers will provide a consistent and isolated environment. 

**Step for docker containerization:** Write the Dockerfile that specifies the base image, sets up the environment, 
and defines how to run the application

## Monitoring

For a comprehensive monitoring and visualization system for the application [Grafana](https://grafana.com/) and [Prometheus](https://prometheus.io/) will be used. 




